//Start an Angualar app
//IIFE - immediately invoked function expression
(function() {
    //Create an instance of my Angualar app
    var CartApp = angular.module("CartApp", ["ngAnimate"]);

    //Define a function to be used as controller
    var CartCtrl = function() {
        var cartCtrl = this;

        cartCtrl.item = "";
        cartCtrl.quantity = 0;
        cartCtrl.filterText = "";
        cartCtrl.basket = [];

        cartCtrl.addToBasket = function() {
            //Add item to basket
            var idx = cartCtrl.basket.findIndex(function(elem) {
                return (elem.item == cartCtrl.item);
            });
            
            if (-1 == idx)
                cartCtrl.basket.push({
                    item: cartCtrl.item,
                    quantity: cartCtrl.quantity
                });
            else
                cartCtrl.basket[idx].quantity += cartCtrl.quantity;
            //Reset the model
            cartCtrl.item = "";
            cartCtrl.quantity = 0;
        };
    }

    //Define the controller
    CartApp.controller("CartCtrl", [ CartCtrl ]);

})();